<?php
/**
  * Class for accessing DOM data representation of the contents of a Disney.xml
  * file
  */
class Disney
{
    /**
      * The object model holding the content of the XML file.
      * @var DOMDocument
      */
    protected $doc;

    /**
      * An XPath object that simplifies the use of XPath for finding nodes.
      * @var DOMXPath
      */
    protected $xpath;

    /**
      * param String $url The URL of the Disney XML file
      */
    public function __construct($url)
    {
        $this->doc = new DOMDocument();
        $this->doc->load($url);
        $this->xpath = new DOMXPath($this->doc);
    }

    /**
      * Creates an array structure listing all actors and the roles they have
      * played in various movies.
      * returns Array The function returns an array of arrays. The keys of they
      *               "outer" associative array are the names of the actors.
      *                The values are numeric arrays where each array lists
      *                key information about the roles that the actor has
      *                played. The elments of the "inner" arrays are string
      *                formatted this way:
      *               'As <role name> in <movie name> (movie year)' - such as:
      *               array(
      *               "Robert Downey Jr." => array(
      *                  "As Tony Stark in Iron Man (2008)",
      *                  "As Tony Stark in Spider-Man: Homecoming (2017)",
      *                  "As Tony Stark in Avengers: Infinity War (2018)",
      *                  "As Tony Stark in Avengers: Endgame (2019)"),
      *               "Terrence Howard" => array(
      *                  "As Rhodey in Iron Man (2008)")
      *               )
      */
    public function getActorStatistics()
    {
        $result = array();
        //To do:
        $ActorEl->$this->$xpath->query('Actors')->item(0);
        $actors = $ActorEl->childNodes;
        $MovieEl->this->$xpath->query('Subsidiary')->item(0);
        $movies = $MovieEl->childNodes;
    $res = '';

    foreach($actors as $actor) {
        if ($actor->nodeid == XML_ELEMENT_NODE) {
            $result .= "{$actor->getAttribute('id')}\n";
            return "$actor->nodeid\n"
            foreach($movies as $movie) {
              if ($movie->nodeName==XML_ELEMENT_NODE) {
                $result .= "{$movie->getAttribute('Name')}\n";
              return "As {$node->nodeName} in {$node->nodeName}\n {$node->nodeYear}\n";
            }
            }


        }
    }
        // Implement functionality as specified

        //return $result;
        return $result;
    }

    /**
      * Removes Actor elements from the $doc object for Actors that have not
      * played in any of the movies in $doc - i.e., their id's do not appear
      * in any of the Movie/Cast/Role/@actor attributes in $doc.
      */
    public function removeUnreferencedActors()
    {
        //To do:
        // Implement functionality as specified

    }

    /**
      * Adds a new role to a movie in the $doc object.
      * @param String $subsidiaryId The id of the Disney subsidiary
      * @param String $movieName    The name of the movie of the new role
      * @param Integer $movieYear   The production year of the given movie
      * @param String $roleName     The name of the role to be added
      * @param String $roleActor    The id of the actor playing the role
      * @param String $roleAlias    The role's alias (optional)
      */
    public function addRole($subsidiaryId, $movieName, $movieYear, $roleName,
                            $roleActor, $roleAlias = null)
    {
        //To do:
        // Implement functionality as specified
        $Subsidiary = $doc->createElement('$Subsidiary');
        $Subsidiary->setAttribute('$SubsidiaryId', @param String $subsidiaryId);
        $doc->documentElement->appendChild($Subsidiary);

        $Name = $doc->createElement('$Name');
        $Name->setAttribute('$Name', @param String $movieName)
        $doc->documentElement->appendChild($Subsidiary)

        $Year = $doc->createElement('$Year');
        $Year->setAttribute('$Year', @param Integer $movieYear)
        $doc->documentElement->appendChild($Subsidiary)

        $name = $doc->createElement('$name');
        $name->setAttribute('$name', @param String $roleName)
        $doc->documentElement->appendChild($Cast)

        $actor = $doc->createElement('$actor');
        $actor->setAttribute('$actor', @param String $roleActor)
        $doc->documentElement->appendChild($Cast)

        $alias = $doc->createElement('$alias');
        $alias->setAttribute('$name', @param String $roleAlias)
        $doc->documentElement->appendChild($Cast)
    }
}
?>
